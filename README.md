# Teslaspole

Dokumentasjon for eit design av ein DRSSTC.

## Spesifikasjonar

* Type: DRSSTC
* Drivar: [Steve Ward's Universal DRSSTC Driver](https://www.loneoceans.com/labs/ud27/)
* Interrupter: [midi2](https://www.loneoceans.com/labs/sales/midi2/)

## Lesestoff

[Kaizer power electronics DRSSTC design guide](https://kaizerpowerelectronics.dk/tesla-coils/drsstc-design-guide/)
[HVtesla](http://www.hvtesla.com/design.html)
[JavaTC berekningsprogram](http://www.classictesla.com/java/javatc/javatc.html)

